import { Recipe } from '../interfaces/Recipe';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import Link from 'next/link';

interface RecipeSliderProps {
  recipes: Recipe[];
}

const RecipeSlider = ({ recipes }: RecipeSliderProps) => {
  return (
    <Carousel
      showThumbs={false}
      showStatus={false}
      autoPlay
      infiniteLoop
      emulateTouch
    >
      {recipes.map((recipe) => (
        <div className='slide-container' key={`slider-${recipe._id}`}>
          <img src={recipe.image} alt='recipe' className='slide-image' />
          <Link href={`/recipes/${recipe._id}`} passHref>
            <div className='legend'>
              <div>{recipe.title}</div>
              <div>Duration: {recipe.duration}</div>
            </div>
          </Link>
        </div>
      ))}
    </Carousel>
  );
};

export default RecipeSlider;
