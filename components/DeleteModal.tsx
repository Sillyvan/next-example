interface DeleteModalProps {
  isModalOpen: boolean;
  closeDeleteModal: () => void;
  selectedRecipe: number | null;
  deleteRecipe: (id: number) => void;
}

const DeleteModal = ({
  isModalOpen,
  closeDeleteModal,
  selectedRecipe,
  deleteRecipe,
}: DeleteModalProps) => {
  return (
    <div className={`modal ${isModalOpen && 'is-active'}`}>
      <div className='modal-background' onClick={closeDeleteModal}></div>
      <div className='modal-card'>
        <section className='modal-card-body content section'>
          <h1 className='title'>
            Are you sure you want to delete this recipe?
          </h1>
          <p className='subtitle'>This cant be undone!</p>
          <button
            className='button is-danger'
            onClick={() => {
              selectedRecipe !== null && deleteRecipe(selectedRecipe);
              closeDeleteModal();
            }}
          >
            Delete
          </button>
        </section>
      </div>
      <button
        className='modal-close is-large'
        aria-label='close'
        onClick={closeDeleteModal}
      ></button>
    </div>
  );
};

export default DeleteModal;
