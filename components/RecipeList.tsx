import 'bulma-list/css/bulma-list.css';
import { useState } from 'react';
import Link from 'next/link';
import { Recipe } from '../interfaces/Recipe';
import DeleteModal from './DeleteModal';
import useRecipe from '../hooks/useRecipe';

interface RecipeListProps {
  recipes: Recipe[];
}

const RecipeList = ({ recipes }: RecipeListProps) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedRecipe, setSelectedRecipe] = useState<null | number>(null);
  const { deleteRecipe } = useRecipe();

  const openDeleteModal = (id: number) => {
    setIsModalOpen(true);
    setSelectedRecipe(id);
  };

  const closeDeleteModal = () => {
    setIsModalOpen(false);
  };

  return (
    <div>
      <DeleteModal
        closeDeleteModal={closeDeleteModal}
        deleteRecipe={deleteRecipe}
        isModalOpen={isModalOpen}
        selectedRecipe={selectedRecipe}
      />
      {/* Adding every recipe to the dom */}
      {recipes.map((recipe) => (
        <div className='list' key={`list-${recipe._id}`}>
          <div className='list-item'>
            <Link href={`/recipes/${recipe._id}`} passHref>
              <div className='list-item-image'>
                <figure className='image is-64x64'>
                  <img alt='recipe' src={recipe.image} className='list-image' />
                </figure>
              </div>
            </Link>

            <div className='list-item-content'>
              <Link href={`/recipes/${recipe._id}`} passHref>
                <div className='list-item-title'>{recipe.title}</div>
              </Link>
              <div className='list-item-description'>
                <div className='tag is-rounded'>
                  <span className='icon is-small'>
                    <img src='/clock.svg' alt='clock icon' />
                  </span>
                  <span>{recipe.duration}</span>
                </div>
              </div>
            </div>
            <div className='list-item-controls'>
              <div className='buttons is-right'>
                <button
                  className='button is-danger'
                  onClick={() => openDeleteModal(recipe._id)}
                >
                  Delete
                </button>
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default RecipeList;
