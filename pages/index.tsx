import { NextPage } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import { useState } from 'react';
import styles from '../styles/Landingpage.module.css';

const Landingpage: NextPage = () => {
  const [isMobileNav, setIsMobileNav] = useState(false);

  const toggleMobileNav = () => {
    setIsMobileNav((state) => !state);
  };

  return (
    <>
      <Head>
        <title>Landingpage</title>
        <meta name='description' content='Landingpage for homework' />
      </Head>
      <nav className='navbar'>
        <div className='navbar-brand'>
          <div className='navbar-item'>
            <h1 className='title'>Landingpage</h1>
          </div>
          <div
            onClick={toggleMobileNav}
            className={`navbar-burger burger ${isMobileNav && 'is-active'}`}
            data-target='navbarExampleTransparentExample'
          >
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>

        <div
          id='navbarExampleTransparentExample'
          className={`navbar-menu ${isMobileNav && 'is-active'}`}
        >
          <div className='navbar-start'>
            <a className='navbar-item'>Home</a>

            <div className='navbar-item has-dropdown is-hoverable'>
              <a className='navbar-link'>More</a>
              <div className='navbar-dropdown is-boxed'>
                <a className='navbar-item'>Lorem ipsum</a>
              </div>
            </div>
          </div>

          <div className='navbar-end'>
            <div className='navbar-item'>
              <div className='field'>
                <div className='control'>
                  <Link href='/recipes' passHref>
                    <button className='button is-primary'>Get Started</button>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </nav>

      <section className='hero mb-6'>
        <figure className='image'>
          <img
            src='https://www.authentic-kitchen.de/wp/wp-content/uploads/2020/04/Noodles_Authentic_Kitchen_Furniture-12.jpg'
            className={styles.banner}
            alt='banner'
          />
        </figure>
      </section>

      <section className='section'>
        <div className='container has-text-centered'>
          <h2 className='title'>Placeholder</h2>
          <p>
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nulla
            quisquam tenetur neque ut deserunt, animi quos officia delectus,
            tempora iusto amet. Suscipit tempora ex inventore optio dignissimos
            dolor, qui velit? Optio facere quisquam voluptatem cum nostrum
            maxime, veritatis, vero eos laborum molestias magnam molestiae
            fugit. Facere ratione accusamus expedita! Quas et eius est eum
            similique illum odio nostrum labore nesciunt. Molestiae aliquid
            mollitia sit eos quos est recusandae itaque, obcaecati reiciendis
            blanditiis. Deserunt, maiores eaque delectus provident, explicabo
            ipsam pariatur sit quam dolorem dicta maxime cupiditate tenetur?
            Animi, voluptas dolores. Earum omnis, velit hic exercitationem nobis
            natus unde tempora! Impedit voluptates molestiae deserunt maxime?
            Ad, delectus reiciendis rem reprehenderit laborum blanditiis officia
            nostrum labore corrupti repellat totam impedit doloremque! Veniam.
            Eos iste laudantium molestias, itaque vero explicabo doloribus! Ad
            ab sit officia doloremque magnam odio quos eveniet rerum ratione
            dolore, dolorum minima quis? Nam at in molestiae mollitia veritatis
            iure.
          </p>

          <div className='columns is-centered p-5'>
            <div className='column'>
              <div className='card'>
                <div className='card-image'>
                  <figure className='image is-2by1'>
                    <img
                      src='https://images.pexels.com/photos/167635/pexels-photo-167635.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'
                      alt='Placeholder image'
                    />
                  </figure>
                </div>
                <div className='card-content'>
                  <div className='media'>
                    <div className='media-content'>
                      <p className='title is-4'>Placeholder</p>
                      <p className='subtitle is-6'>@placeholder</p>
                    </div>
                  </div>

                  <div className='content'>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Phasellus nec iaculis mauris.
                  </div>
                </div>
              </div>
            </div>
            <div className='column'>
              <div className='card'>
                <div className='card-image'>
                  <figure className='image is-2by1'>
                    <img
                      src='https://images.pexels.com/photos/167635/pexels-photo-167635.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'
                      alt='Placeholder image'
                    />
                  </figure>
                </div>
                <div className='card-content'>
                  <div className='media'>
                    <div className='media-content'>
                      <p className='title is-4'>Placeholder</p>
                      <p className='subtitle is-6'>@placeholder</p>
                    </div>
                  </div>

                  <div className='content'>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Phasellus nec iaculis mauris.
                  </div>
                </div>
              </div>
            </div>
            <div className='column'>
              <div className='card'>
                <div className='card-image'>
                  <figure className='image is-2by1'>
                    <img
                      src='https://images.pexels.com/photos/167635/pexels-photo-167635.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'
                      alt='Placeholder image'
                    />
                  </figure>
                </div>
                <div className='card-content'>
                  <div className='media'>
                    <div className='media-content'>
                      <p className='title is-4'>Placeholder</p>
                      <p className='subtitle is-6'>@placeholder</p>
                    </div>
                  </div>

                  <div className='content'>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Phasellus nec iaculis mauris.
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className='section'>
        <div className='container has-text-centered'>
          <h2 className='title'>Contact</h2>

          <form>
            <div className='field is-horizontal'>
              <div className='field-body'>
                <div className='field'>
                  <p className='control '>
                    <input className='input' type='text' placeholder='Name' />
                  </p>
                </div>
                <div className='field'>
                  <p className='control'>
                    <input className='input' type='email' placeholder='Email' />
                  </p>
                </div>
              </div>
            </div>

            <div className='field is-horizontal'>
              <div className='field-body'>
                <div className='field'>
                  <div className='control'>
                    <textarea
                      className='textarea'
                      placeholder='Message us'
                    ></textarea>
                  </div>
                </div>
              </div>
            </div>

            <div className='field is-horizontal'>
              <div className='field-body'>
                <div className='field'>
                  <div className='control'>
                    <button className='button is-primary'>Send message</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </section>
      <footer className='footer'>
        <div className='container'>
          <div className='content has-text-right'>
            <p>
              Made by:{' '}
              <a href='https://gehrig.dev' target='_blank' rel='noreferrer'>
                Silvan Gehrig
              </a>
            </p>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Landingpage;
