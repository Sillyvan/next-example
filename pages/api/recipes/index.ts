import type { NextApiRequest, NextApiResponse } from 'next';

import dbConnect from '../../../dbConnect';
import Recipe from '../../../models/Recipe';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { method } = req;

  await dbConnect();

  switch (method) {
    case 'GET':
      try {
        const recipes = await Recipe.find({});
        res.status(200).json({ status: 'success', data: recipes });
      } catch (error) {
        res.status(400).json({ status: 'failed', error });
      }
      break;
    case 'POST':
      try {
        const recipe = new Recipe(req.body);
        await recipe.save();
        res.status(200).json({ status: 'success' });
      } catch (error) {
        res.status(400).json({ status: 'failed', error });
      }
      break;
    case 'DELETE':
      try {
        await Recipe.findByIdAndDelete(req.query.id);
        res.status(200).json({ success: true, deleted: req.query.id });
      } catch (error) {
        res.status(400).json({ status: 'failed', error });
      }
      break;
    case 'PUT':
      try {
        await Recipe.findByIdAndUpdate(req.query.id, req.body);
        res.status(200).json({ status: 'success' });
      } catch (error) {
        res.status(400).json({ status: 'failed', error });
      }
      break;
    default:
      res.status(400).json({ status: 'failed' });
      break;
  }
}
