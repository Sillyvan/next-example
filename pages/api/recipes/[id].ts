import type { NextApiRequest, NextApiResponse } from 'next';

import dbConnect from '../../../dbConnect';
import Recipe from '../../../models/Recipe';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { id } = req.query;
  await dbConnect();

  try {
    const recipe = await Recipe.findById(id);
    res.status(200).json({ status: 'success', data: recipe });
  } catch (error) {
    res.status(400).json({ status: 'failed', error });
  }
}
