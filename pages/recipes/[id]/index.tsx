import { GetServerSideProps, NextPage } from 'next';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { Recipe } from '../../../interfaces/Recipe';

interface RecipePageProps {
  recipe: Recipe;
}

const RecipePage = ({ recipe }: RecipePageProps) => {
  const {
    query: { id },
  } = useRouter();

  return (
    <section className='section'>
      <div className='container content'>
        {recipe ? (
          <>
            <div className='title-container'>
              <h1 className='title'>{recipe.title}</h1>

              <Link href='/recipes  ' passHref>
                <button className='button is-link'>Back</button>
              </Link>
            </div>
            <div className='conent'>
              <img src={`/${recipe.image}`} alt='recipe' />
              <h2 className='subtitle'>Druation: {recipe.duration}</h2>
              <h2 className='subtitle'>Instructions</h2>
              <p>{recipe.instructions}</p>
            </div>
            <Link href={`/recipes/${id}/edit`} passHref>
              <button className='button is-link'>Edit</button>
            </Link>
          </>
        ) : (
          <p>lol</p>
        )}
      </div>
    </section>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  const req = await fetch(
    `${process.env.APPLICATION_URI}/api/recipes/${params!.id}`
  );
  const recipe = await req.json();

  return { props: { recipe: recipe.data } };
};

export default RecipePage;
