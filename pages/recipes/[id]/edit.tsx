import { Field, Formik } from 'formik';
import { GetServerSideProps, NextPage } from 'next';
import { useRouter } from 'next/router';
import { EditRecipeValidation } from '../../../validations/EditRecipeValidation';
import Link from 'next/link';
import { Recipe } from '../../../interfaces/Recipe';
import useRecipe from '../../../hooks/useRecipe';

interface RecipeEditProps {
  recipe: Recipe;
  url: string;
}

const RecipeEdit = ({ recipe, url }: RecipeEditProps) => {
  const {
    query: { id },
  } = useRouter();
  const { editRecipe } = useRecipe();
  return (
    <section className='section'>
      <div className='container'>
        <div className='title-container'>
          <h1 className='title'>Add Recipe</h1>
          <Link href='/recipes' passHref>
            <button className='button is-link'>Back</button>
          </Link>
        </div>

        <Formik
          initialValues={{
            title: recipe.title,
            instructions: recipe.instructions,
            duration: recipe.duration,
            image: `${url}/${recipe.image}`,
          }}
          validate={EditRecipeValidation}
          onSubmit={(values) => {
            editRecipe(String(id), values);
          }}
        >
          {({ values, touched, handleSubmit, errors }) => (
            <form onSubmit={handleSubmit}>
              <div className='field'>
                <label className='label'>Title*</label>
                <div className='control'>
                  <Field
                    className={`input ${
                      errors.title && touched.title && 'is-danger'
                    }`}
                    type='text'
                    name='title'
                    value={values.title}
                  />
                </div>
                {errors.title && touched.title && (
                  <p className='has-text-danger'>{errors.title}</p>
                )}
              </div>
              <div className='field'>
                <label className='label'>Instructions*</label>
                <div className='control'>
                  <Field
                    className={`input ${
                      errors.instructions && touched.instructions && 'is-danger'
                    }`}
                    type='text'
                    name='instructions'
                    value={values.instructions}
                  />
                </div>
                {errors.instructions && touched.instructions && (
                  <p className='has-text-danger'>{errors.instructions}</p>
                )}
              </div>
              <div className='field'>
                <label className='label'>Duration*</label>
                <div className='control'>
                  <Field
                    className={`input ${
                      errors.duration && touched.duration && 'is-danger'
                    }`}
                    type='text'
                    name='duration'
                    value={values.duration}
                  />
                </div>
                {errors.duration && touched.duration && (
                  <p className='has-text-danger'>{errors.duration}</p>
                )}
              </div>
              <div className='field'>
                <label className='label'>Image*</label>
                <div className='control'>
                  <Field
                    className={`input ${
                      errors.image && touched.image && 'is-danger'
                    }`}
                    type='url'
                    name='image'
                    value={values.image}
                  />
                </div>
                {errors.image && touched.image && (
                  <p className='has-text-danger'>{errors.image}</p>
                )}
              </div>
              {/* disable submit button if not all fields are filled */}
              {values.title &&
              values.instructions &&
              values.duration &&
              values.image ? (
                <button className='button is-link' type='submit'>
                  Edit Recipe
                </button>
              ) : (
                <button className='button is-link' type='submit' disabled>
                  Edit Recipe
                </button>
              )}
            </form>
          )}
        </Formik>
      </div>
    </section>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  const req = await fetch(
    `${process.env.APPLICATION_URI}/api/recipes/${params!.id}`
  );
  const recipe = await req.json();

  return { props: { recipe: recipe.data, url: process.env.APPLICATION_URI } };
};

export default RecipeEdit;
