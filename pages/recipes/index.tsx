import { GetServerSideProps } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import RecipeList from '../../components/RecipeList';
import RecipeSlider from '../../components/RecipeSlider';
import { Recipe } from '../../interfaces/Recipe';

interface RecipePageProps {
  recipes: Recipe[];
}

const RecipePage = ({ recipes }: RecipePageProps) => {
  return (
    <>
      <Head>
        <title>Recipes</title>
        <meta name='description' content='Recipes' />
      </Head>
      <section className='section'>
        <div className='container'>
          <div className='title-container'>
            <h1 className='title'>Home</h1>
            <Link href='/recipes/add' passHref>
              <button className='button is-link'>Add Recipe</button>
            </Link>
          </div>
          {recipes.length > 0 ? (
            <>
              <RecipeSlider recipes={recipes} />
              <RecipeList recipes={recipes} />
            </>
          ) : (
            <h2 className='subtitles'>No Recipes found!</h2>
          )}
        </div>
      </section>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const req = await fetch(`${process.env.APPLICATION_URI}/api/recipes`);
  const recipes = await req.json();

  return { props: { recipes: recipes.data } };
};

export default RecipePage;
