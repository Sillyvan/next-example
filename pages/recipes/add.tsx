import { Field, Formik } from 'formik';
import { NextPage } from 'next';
import { AddRecipeValidation } from '../../validations/AddRecipeValidation';
import Link from 'next/link';
import useRecipe from '../../hooks/useRecipe';

const RecipeAdd: NextPage = () => {
  const { addRecipe } = useRecipe();
  return (
    <section className='section'>
      <div className='container'>
        <div className='title-container'>
          <h1 className='title'>Add Recipe</h1>
          <Link href='/recipes' passHref>
            <button className='button is-link'>Back</button>
          </Link>
        </div>

        <Formik
          initialValues={{
            title: '',
            instructions: '',
            duration: '',
            image: '',
          }}
          validate={AddRecipeValidation}
          onSubmit={(values) => {
            addRecipe(values);
          }}
        >
          {({ values, touched, handleSubmit, errors }) => (
            <form onSubmit={handleSubmit}>
              <div className='field'>
                <label className='label'>Title*</label>
                <div className='control'>
                  <Field
                    className={`input ${
                      errors.title && touched.title && 'is-danger'
                    }`}
                    type='text'
                    name='title'
                    value={values.title}
                  />
                </div>
                {errors.title && touched.title && (
                  <p className='has-text-danger'>{errors.title}</p>
                )}
              </div>
              <div className='field'>
                <label className='label'>Instructions*</label>
                <div className='control'>
                  <Field
                    className={`input ${
                      errors.instructions && touched.instructions && 'is-danger'
                    }`}
                    type='text'
                    name='instructions'
                    value={values.instructions}
                  />
                </div>
                {errors.instructions && touched.instructions && (
                  <p className='has-text-danger'>{errors.instructions}</p>
                )}
              </div>
              <div className='field'>
                <label className='label'>Duration*</label>
                <div className='control'>
                  <Field
                    className={`input ${
                      errors.duration && touched.duration && 'is-danger'
                    }`}
                    type='text'
                    name='duration'
                    value={values.duration}
                  />
                </div>
                {errors.duration && touched.duration && (
                  <p className='has-text-danger'>{errors.duration}</p>
                )}
              </div>
              <div className='field'>
                <label className='label'>Image*</label>
                <div className='control'>
                  <Field
                    className={`input ${
                      errors.image && touched.image && 'is-danger'
                    }`}
                    type='url'
                    name='image'
                    value={values.image}
                  />
                </div>
                {errors.image && touched.image && (
                  <p className='has-text-danger'>{errors.image}</p>
                )}
              </div>
              {/* disable submit button if not all fields are filled */}
              {values.title &&
              values.instructions &&
              values.duration &&
              values.image ? (
                <button className='button is-link' type='submit'>
                  Add Recipe
                </button>
              ) : (
                <button className='button is-link' type='submit' disabled>
                  Add Recipe
                </button>
              )}
            </form>
          )}
        </Formik>
      </div>
    </section>
  );
};

export default RecipeAdd;
