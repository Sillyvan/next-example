import mongoose from 'mongoose';

const Recipe2 = new mongoose.Schema({
  title: { type: String, required: true },
  instructions: { type: String, required: true },
  duration: { type: String, required: true },
  image: { type: String, required: true },
});

export default mongoose.models.Recipe || mongoose.model('Recipe', Recipe2);
