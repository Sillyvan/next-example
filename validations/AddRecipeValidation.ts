import { AddRecipe } from '../interfaces/Recipe';

export const AddRecipeValidation = ({
  title,
  instructions,
  duration,
  image,
}: AddRecipe) => {
  const errors: AddRecipe = {
    title: '',
    instructions: '',
    duration: '',
    image: '',
  };

  if (!title) errors.title = 'Email Required';
  if (!instructions) errors.instructions = 'Instructions Required';
  if (!duration) errors.duration = 'Duration Required';
  if (!image) errors.image = 'Image Required';

  // Formik expcets an empty object if everything is correct
  if (title && instructions && duration && image) return {};

  return errors;
};
