export interface Recipe {
  _id: number;
  title: string;
  duration: string;
  instructions: string;
  image: string;
}

export interface AddRecipe {
  title: string;
  duration: string;
  instructions: string;
  image: string;
}

export interface EditRecipe {
  title: string;
  duration: string;
  instructions: string;
  image: string;
}
