import { AddRecipe, EditRecipe } from '../interfaces/Recipe';
import Router from 'next/router';

const useRecipe = () => {
  const addRecipe = async (recipe: AddRecipe) => {
    console.log(recipe);
    await fetch(`/api/recipes`, {
      method: 'POST',
      body: JSON.stringify(recipe),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    Router.replace('/recipes');
  };

  const editRecipe = async (id: string, recipe: EditRecipe) => {
    await fetch(`/api/recipes?id=${id}`, {
      method: 'PUT',
      body: JSON.stringify(recipe),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    Router.replace('/recipes');
  };

  const deleteRecipe = async (id: number) => {
    await fetch(`/api/recipes?id=${id}`, { method: 'DELETE' });
    Router.reload();
  };

  return { addRecipe, editRecipe, deleteRecipe };
};

export default useRecipe;
